package com.epam.datahandling.util;

/**
 * Utility class for low-level work with console
 */
public class ConsoleUtils {
    
    private ConsoleUtils() {
        
    }
    
    /**
     * Read line from console. Line is sequence of symbols ending with new line symbol
     * @return line that was read from console
     */
    public static String readLine() {
        throw new UnsupportedOperationException("Implement this method");
    }
    
    /**
     * Read number from console
     * @return number that was read from console
     */
    public static Integer readNumber() {
        throw new UnsupportedOperationException("Implement this method");
    }

}
