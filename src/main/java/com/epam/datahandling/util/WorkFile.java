package com.epam.datahandling.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class WorkFile {

	public static void setText(File file) {
		FileWriter writer = null;
		String text = "";
		try {
			text = getTextFromFile(file);
			ResourceBundle pattString = ResourceBundle.getBundle("regular_expressions");
			text = text.replaceAll(pattString.getString("patternForFileProcessing"), " ");
			writer = new FileWriter(file);
			writer.write(text);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getTextFromFile(File originalFile) {
		char[] text = new char[(int) originalFile.length()];
		FileReader inStream = null;
		try {
			inStream = new FileReader(originalFile);
			inStream.read(text);
		} catch (IOException e) {
			System.out.println("IOException");
		} finally {
			try {
				inStream.close();
			} catch (IOException e) {
				System.out.println("Stream is not open");
			}
		}
		return new String(text);
	}

	public static void createBackup(File originFile) {
		byte[] array = new byte[(int) originFile.length()];
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
		try {
			inStream=new FileInputStream(originFile);
			inStream.read(array);
			File backup=new File("src\\main\\resources\\backup\\backup.txt");
			outStream=new FileOutputStream(backup);
			outStream.write(array);
		}catch (IOException e) {
			System.out.println("IOException");
		} finally {
			try {
				inStream.close();
				outStream.close();
			} catch (IOException e) {
				System.out.println("Stream is not open");
			}
	}
}
}
