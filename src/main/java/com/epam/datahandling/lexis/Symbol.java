package com.epam.datahandling.lexis;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Symbol {
	public static StringBuilder findeAllVowels(String text) {
		Pattern pattern = null;
		ResourceBundle bundle = null;
		text = text.toLowerCase();
		try {
			bundle = ResourceBundle.getBundle("regular_expressions");
			String string = bundle.getString("patternForVowels");
			pattern = Pattern.compile(string);
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		Matcher matcher = pattern.matcher(text);
		StringBuilder builder = new StringBuilder("");
		while (matcher.find()) {
			builder.append(matcher.group());
		}
		return builder;
	}
	public static char findMoreVowels(String text) {
		StringBuilder string= findeAllVowels(text);
		int count=0;
		int maxValue=Integer.MIN_VALUE;
		char[] arrChar = { 'a', 'e', 'o', 'u', 'y', 'i' };
		char symbol='@';
		for(int i=0;i<arrChar.length;i++) {
			for (int j = 0; j < string.length(); j++) {
				if(arrChar[i]==string.charAt(j)) {
					count++;
				}
				
			}
			if(count>maxValue) {
				maxValue=count;
				symbol=string.charAt(i);
			}
		}
		return symbol;
	}
}
