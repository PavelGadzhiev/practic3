package com.epam.datahandling.lexis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Sentence is a sequence of characters separated by ".", "!", "?" and new line
 * characters
 */
public class Sentence implements Comparable<Sentence> {
	private String text;
	private int countWord;

	public Sentence(String text) {
		this.text = text;
		countWord = Word.counterWords(text);
	}

	public static int CounterSentence(String text) {
		int count = 0;
		ResourceBundle bundle = null;
		Pattern pattern = null;
		try {
			bundle = ResourceBundle.getBundle("regular_expressions");
			pattern = Pattern.compile(bundle.getString("patternOfSentence"));
		} catch (MissingResourceException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			count++;
		}
		return count;
	}

	public static Sentence[] findSentences(String text) {
		Sentence[] sentences = null;
		ResourceBundle bundle = null;
		Pattern pattern = null;
		try {
			bundle = ResourceBundle.getBundle("regular_expressions");
			pattern = Pattern.compile(bundle.getString("patternOfSentence"));
		} catch (MissingResourceException e) {
			// TODO: handle exception
			e.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Matcher matcher = pattern.matcher(text);
		sentences = new Sentence[CounterSentence(text)];
		int i = 0;
		while (matcher.find()) {
			sentences[i] = new Sentence(matcher.group());
			i++;
		}
		return sentences;
	}

	public String getText() {
		return this.text;
	}

	public static void SentenceInFile(File file, Sentence[] sentences) {
		Arrays.sort(sentences);
		FileWriter fileWriter = null;
		BufferedWriter bufferedWriter = null;
		PrintWriter printWriter = null;
		try {
			fileWriter = new FileWriter(file, true);
			bufferedWriter = new BufferedWriter(fileWriter);
			printWriter = new PrintWriter(bufferedWriter);
			printWriter.println();
			printWriter.println("|====================================================================================|");
			printWriter.println("|  №  |                           Sentence                            | Word's Count |");
			printWriter.println("|=====+===============================================================+==============|");
			for (int i = 0; i < sentences.length; i++) {
				printWriter.printf("%s%2d%s", "| ", i + 1, "  | ");
				Word[] words = Word.findWord(sentences[i].getText());
				StringBuilder stringBuilder = new StringBuilder(" ");
				for (Word word : words) {
					if (stringBuilder.length() + word.getLength() > 25) {
						break;
					}
					stringBuilder = stringBuilder.append(word.getText() + " ");
				}
				printWriter.printf("%-61s%s%2d%s%n", stringBuilder, " |      ", sentences[i].getCountWord(), "      |");
				printWriter.println("|------------------------------------------------------------------------------------|");

			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		finally {
			printWriter.close();
		}
	}

	public int getCountWord() {
		return this.countWord;
	}

	@Override
	public int compareTo(Sentence o) {
		if(o.countWord>countWord){
			return -1;
		}
		if(o.countWord<countWord){
			return 1;
		}
		return 0;
	}
}
