package com.epam.datahandling.lexis;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Word is a sequence of characters separated by spaces and punctuation
 */
public class Word {

	private String text = null;

	public Word(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public int getLength() {
		return this.text.length();
	}

	public static int counterWords(String text) {
		int count = 0;
		Pattern pattern = null;
		ResourceBundle path = null;
		try {
			path = ResourceBundle.getBundle("regular_expressions");
			pattern = Pattern.compile(path.getString("patternOfWord"));
		} catch (MissingResourceException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			count++;
		}
		return count;
	}

	public static Word[] findWord(String text) {
		Word[] words;
		Pattern pattern = null;
		ResourceBundle path = null;
		try {
			path = ResourceBundle.getBundle("regular_expressions");
			pattern = Pattern.compile(path.getString("patternOfWord"));
		} catch (MissingResourceException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		Matcher matcher = pattern.matcher(text);
		words = new Word[counterWords(text)];
		int i = 0;
		while (matcher.find()) {
			words[i] = new Word(matcher.group());
			i++;
		}
		return words;
	}

	public static void writeSerial(Word[] word) {
		FileOutputStream outputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			outputStream = new FileOutputStream("src\\main\\resources\\outputFile.out");
			objectOutputStream = new ObjectOutputStream(outputStream);
			objectOutputStream.writeObject(outputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				objectOutputStream.flush();
				objectOutputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	public static Word[] readSerial() {
		FileInputStream inputStream = null;
		ObjectInputStream objectInputStream = null;
		Word[] words = null;
		try {
			inputStream = new FileInputStream("src\\main\\resources\\outputFile.out");
			objectInputStream = new ObjectInputStream(inputStream);
			words = (Word[]) objectInputStream.readObject();
		} catch (FileNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				objectInputStream.close();
			} catch (IOException e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}
		}
		return words;

	}
}
