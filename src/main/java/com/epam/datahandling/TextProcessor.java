package com.epam.datahandling;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import com.epam.datahandling.lexis.Sentence;
import com.epam.datahandling.lexis.Symbol;
import com.epam.datahandling.util.WorkFile;

/**
 * Demo class demonstrating program capabilities of working with files, text
 * parsing and displaying reports
 */
public class TextProcessor {

	public static void main(String[] args) {
		TextProcessor textProcessor = new TextProcessor();
		textProcessor.start();
	}

	private void start() {
		File file=new File("src\\main\\resources\\original\\book.txt");
		WorkFile.createBackup(file);
		WorkFile.setText(file);
		String text=WorkFile.getTextFromFile(file);
		Sentence[] sentences=Sentence.findSentences(text);
		Sentence.SentenceInFile(file, sentences);
		char vowel= Symbol.findMoreVowels(text);
		System.out.println(vowel);
		
	}

}
